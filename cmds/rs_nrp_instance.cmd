require rs_nrpxxsn,master

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("IP", "172.30.155.12")
epicsEnvSet("secsub", "SRFLab-010")
epicsEnvSet("disdevidx", "PM01-Pick_up")

epicsEnvSet("ASYN_PORT", "rs_nrpxxsn")

epicsEnvSet("EPICS_CAS_SERVER_PORT", "5068")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "65536")

iocshLoad("$(rs_nrp_DIR)/rs_nrpxxsn.iocsh", "secsub=$(secsub), disdevidx=$(disdevidx), IP=$(IP), ASYN_PORT=$(ASYN_PORT)")

iocInit

dbl > "$(TOP)/PVs.list"
